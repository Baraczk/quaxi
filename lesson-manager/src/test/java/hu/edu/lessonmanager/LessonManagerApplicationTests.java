package hu.edu.lessonmanager;

import hu.edu.lessonmanager.config.H2TestProfileJPAConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
  LessonManagerApplication.class,
  H2TestProfileJPAConfig.class
})
@ActiveProfiles("test")
public class LessonManagerApplicationTests {

	@Test
	public void contextLoads() {
	}

}
