import { Component, OnInit } from '@angular/core';
import {Student} from "../student";

@Component({
  selector: 'app-students',
  template: `
    <h2>{{student.name | uppercase}} Details</h2>
    <div><span>id: </span>{{student.id}}</div>
    <div>
      <label>name:
        <input [(ngModel)]="student.name" placeholder="name"/>
      </label>
    </div>
  `,
  styleUrls: ['./students.component.scss']
})
export class StudentsComponent implements OnInit {
  student: Student = {
    id: 1,
    name: 'Béla'
  };

  constructor() { }

  ngOnInit() {
  }

}
